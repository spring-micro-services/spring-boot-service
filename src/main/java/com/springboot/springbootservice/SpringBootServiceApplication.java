package com.springboot.springbootservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

@SpringBootApplication
@EnableEurekaClient
@EnableConfigurationProperties
@RestController
@RefreshScope
public class SpringBootServiceApplication {

  private Logger log = Logger.getLogger(SpringBootServiceApplication.class.getName());

  @Autowired
  private HelloProperties helloProperties;

  @RequestMapping("/hello")
  public String hello() throws UnknownHostException {
    if (helloProperties.isShouldThrowException()) {
      log.info("shouldThrowException is enabled. Throwing forced exception");
      throw new RuntimeException("Forced Exception");
    }

    log.info("Returning hello string for " + helloProperties.getName());
    return String.format("Hello %s from Spring Boot @ %s !", helloProperties.getName(), InetAddress.getLocalHost().getHostName());
  }

  public static void main(String[] args) {
    SpringApplication.run(SpringBootServiceApplication.class, args);
  }
}
